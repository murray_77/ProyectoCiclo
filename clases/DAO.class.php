<?php

Class DAO{
    
    //Conexion con la BD
    private static function getDB() {
        try{
            $dbConnection = new PDO("mysql:host=127.0.0.1;dbname=proyecto", "root", ""); 
            $dbConnection->exec("set names utf8");
            $dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbConnection;
        } catch (PDOException $e){
            echo 'Error en la conexión a la base de datos: ' . $e->getMessage();
            exit();
        }
   }
   
   //Si la sentencia sql es un select se deriva por query, si no, por update. 
   //Ambas pasan por common para preparar la sentencia
   private static function commonExecute($string,$paramArray){   
        $bd = self::getDB();
        $consulta = $bd->prepare($string);
        $consulta->execute($paramArray);
        return $consulta;                   
    }
   
    
    public static function executeUpdate($string,$paramArray){   
        try {            
            self::commonExecute($string,$paramArray);
        }catch (PDOException $e) {
            echo 'Error en la conexión a la base de datos: ' . $e->getMessage();
            exit();
        }
    }
    
    public static function executeQuery($string,$paramArray){   
        try {
            $consulta = self::commonExecute($string,$paramArray);            
            $arrayResultado = array();
            while ($fila = $consulta->fetch(PDO::FETCH_OBJ)) {
                array_push($arrayResultado, $fila);
                
            }
            return $arrayResultado;
        }catch (PDOException $e) {
            echo 'Error en la conexión a la base de datos: ' . $e->getMessage();
            return 0;
        }
    }
    
}

