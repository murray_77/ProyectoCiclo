<?php
require_once 'Persona.class.php';
class Entrenador extends Persona {
    private $permisos;
    private $grupo;   
 
    function getPermisos() {
        return $this->permisos;
    }

    function getGrupoEntrenador(){
        return $this->grupo;
    }
    
    function setPermisos($permisos) {
        $this->permisos = $permisos;
    }
    
    function setGrupoEntrenador($grupo){
        $this->grupo = $grupo;
    }


}
