<?php
include_once 'UserControlador.class.php';
include_once 'Validaciones.class.php';

class Login{    
    
    //validación formulario de login
    public function validaLogin($email, $password){
        $valida = new Validaciones();
        $em = $valida->validaEmail($email);
        $pass = $valida->validaPass($password);       
        
        if ($em != null && $pass != null){
            $this->checkLogin($em, $pass);
        }else{
            header("Location: /proyecto2.0/index.php?error=1");
            die();
        }
    }   
    
    
    //Usuario + contraseña existe
    private function checkLogin($email, $pass){
        
        $usu = new UserControlador();
        $usuario = $usu->getLogin($email, $pass);
        if(empty($usuario)){
            header("Location: /proyecto2.0/index.php?error=2");            
            die();
        }else{
            if($usuario[0]->entrenador == 1){
                $usuario = $usu->getUsuarioById($usuario[0]->id, "entrenador", "id_e");     //usuario es entrenador 
                $this->inicioSesion($usuario);                
                header("Location: /proyecto2.0/php/entrenador/agenda.php");
                die();
            }else{
                $usuario = $usu->getUsuarioById($usuario[0]->id, "cliente", "id_c");   //usuario es cliente
                $this->inicioSesion($usuario);                
                header("Location: /proyecto2.0/php/cliente/agendaCliente.php");
                die();
            }            
        }
    }
    
    
    private function inicioSesion($usuario){
        if(!isset($_SESSION)){ 
            session_start(); 
        }
        $_SESSION["usuario"] = $usuario[0];
    }
       
    
    
    
        


}
