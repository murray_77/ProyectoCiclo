<?php

class Persona {
    private $dni;
    private $nombre;
    private $apellido;
    private $telefono;
    private $email;
    private $pass;
    private $foto;
    private $entrenador;
    
    function getEntrenador() {
        return $this->entrenador;
    }

    function setEntrenador($entrenador){
        $this->entrenador = $entrenador;
    }
    
    function getDni() {
        return $this->dni;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function getPass() {
        return $this->pass;
    }

    function getFoto() {
        return $this->foto;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPass($pass) {
        
        $this->pass = $pass;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }
}