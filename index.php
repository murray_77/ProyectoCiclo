<?php
    /*
    Santiago García Castro

    17/02/2020

    v1.2
     
    Proyecto: Web Entrenador personal
    */
    //admin     santiago@admin.com  abc123    permisos completos
    //admin2    admin2@admin.com    abc123     acceso a la parte administrativa pero sin permisos para modificar la bd  

    //user       user@user.com      abc123    acceso parte cliente
    //user2      user2@user.com     abc123    acceso parte cliente    
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Login.class.php';
    
        if(isset($_POST["acceder"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
            
            $login = new Login();
            $login ->validaLogin($_POST["user"], $_POST["pass"]);  
        }
    
    $smarty->assign("titulo","Inicio");
    $smarty->display('principal/head.tpl');
    $smarty->display('principal/navBar.tpl');       
    $smarty->display('principal/loginModal.tpl');
    $smarty->display('principal/olvidaModal.tpl');
    $smarty->display('principal/index.tpl');    
    $smarty->display('principal/footer.tpl');
    
