<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/UserControlador.class.php';
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if($_SESSION["usuario"]->entrenador!=1){
        header('Location: ../misc/noautorizado.php');
    }
    
    $listaClientes = new UserControlador();
    $resultado = $listaClientes->getAll("cliente","id_c");    
    $smarty->assign('usuarios', $resultado);
    
    $smarty->assign("titulo","Agenda");
    $smarty->display('principal/head.tpl');    
    $smarty->display('entrenador/navEntrenador.tpl');        
    $smarty->display('entrenador/portalAgenda.tpl');   
    $smarty->display('principal/footer.tpl');