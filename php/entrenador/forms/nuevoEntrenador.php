<?php
    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Validaciones.class.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/UserControlador.class.php';
    
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    
    if(($_SESSION["usuario"]->entrenador!=1)||$_SESSION["usuario"]->administrador!=1){
        header('Location: /proyecto2.0/misc/noautorizado.php');
    }    
        
      
    
    
    if(isset($_POST["aceptar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){ /*Validación para creacion*/
			
            $error1 = $error2 = $error3 = $error4 = $error5 = $error6 = 0;

        $validar = new Validaciones();

        $nombre = $validar->validaNombre($_POST["nombre"]);
        $apellido = $validar->validaNombre($_POST["apellido"]);
        $pass = $validar->generaPass();
        $dni = $validar->validaDni($_POST["dni"]);
        $grupo = $validar->validaGrupo($_POST["grupo"]);
        $telefono = $validar->validaTelefono($_POST["telefono"]);
        $email = $validar->validaEmail($_POST["email"]);
        $permisos = $_POST["permisos"];
						
            if($error1 == "" && $error2 == "" && $error3 == "" && $error4 == "" && $error5 == "" && $error6 == ""){
               
                $entrenador = new Entrenador();
                $entrenador->setNombre($nombre);
                $entrenador->setApellido($apellido);
                $entrenador->setDni($dni);
                $entrenador->setGrupoEntrenador($grupo);
                $entrenador->setTelefono($telefono);
                $entrenador->setEmail($email);
                $entrenador->setPass($pass);
                $entrenador->setEntrenador(1);
                $entrenador->setPermisos($permisos);                
                
                $entrenadorDao = new UserControlador();
                $entrenadorDao ->insertEntrenador($entrenador);
                
                header("Location: ../entrenadores.php");
            }else{
                header("Location: registrocliente.php?error1=$error1&error2=$error2&error3=$error3"
                        . "&error4=$error4&error5=$error5&error6=$error6&nombre=$nombre&apellido=$apellido"
                        . "&telefono=$telefono&email=$email&grupo=$grupo&permisos=$permisos");
            }
	}	
        
    $smarty->assign("titulo","Alta Entrenadores");
    $smarty->display('principal/head.tpl');
    $smarty->display('entrenador/navEntrenador.tpl');       
    $smarty->display('entrenador/nuevoEntrenador.tpl');   
    $smarty->display('principal/footer.tpl');