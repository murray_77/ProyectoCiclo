<?php

    require($_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Include.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/proyecto2.0/clases/Login.class.php';
    session_start();
    
    
    if(isset($_POST["acceder"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
            $login = new Login();
            $login ->validaLogin($_POST["user"], $_POST["pass"], $_POST["recordar"]);            
        }
    
    $smarty->assign("titulo","Error");
    $smarty->display("principal/head.tpl");
    $smarty->display('principal/navBar.tpl');    
    $smarty->display('principal/noautorizado.tpl');    
    $smarty->display('principal/footer.tpl');
