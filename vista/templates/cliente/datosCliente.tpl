<main class="container">
    <section>
        <article class="per">
            <h2>Datos Personales</h2>
            <div class="foto"><img class="userfoto rounded-circle" id="userfoto" src="/proyecto/img/profiles/{$smarty.session.usuario.foto}" alt="Imagen Usuario"></div><!--Imagen actual del user o marco vacío-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-row"><strong>Nombre: </strong>{$smarty.session.usuario.nombre}</div></br><!--En los span vacíos se importan los datos de usuario desde la base de datos--> 
                    <div class="form-row"><strong>Apellidos:</strong> {$smarty.session.usuario.apellido}</div></br>
                    <div class="form-row"><strong>DNI:</strong> {$smarty.session.usuario.dni}</div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Fecha de Nacimiento:</strong> {$smarty.session.usuario.fechaN}</div></br>
                    <div class="col-md-12"><strong>Grupo:</strong> {$smarty.session.usuario.grupo}</div></span></br>
                    <form method="post" enctype="multipart/form-data" id="subirfoto" action="" class="col-md-6">
                        <div class="col-form-label"><label for="foto"><strong>Cambiar Foto de Perfil:</strong></label></div>
                        <div class="form-row"><input type="file" name="foto" id="foto"><div class="invalid-feedback"></div></div></br>
                        <div class="from-row"><input type="submit" value="Enviar" name="enviar" class="btn btn-dark"></div>
                    </form>
                </div>
            </div>
        </article></br>

        <article class="cont">
            <h2>Datos de Contacto</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Dirección:</strong> {$smarty.session.usuario.direccion}</div></br><!--En los span vacíos se importan los datos de usuario desde la base de datos--> 
                    <div class="col-md-12"><strong>Población:</strong> {$smarty.session.usuario.poblacion}</div></br>
                    <div class="col-md-12"><strong>CP:</strong> {$smarty.session.usuario.cp}</div></br>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Teléfono:</strong> {$smarty.session.usuario.telefono}</div></br>
                    <div class="col-md-12"><strong>Email:</strong> {$smarty.session.usuario.email}</div></br>
                </div>
            </div>
        </article>
    </section>
    <section class="botones">
            <form method="post" action="php" action="">
                    <input type="submit" class="btn btn-dark" value="Cambiar Contraseña">
                    <input type="submit" class="btn btn-dark" value="Solicitar Baja">
            </form>
    </section>
</main>