<main class="container">
    <h1>Buscador de Usuarios</h1>
    <div class="row">
        <form method="post" class="form-group col-md-6 " acction="{$smarty.server.PHP_SELF}">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Introduce nombre o DNI" name="buscar" aria-descr>
                <div class="input-group-append">
                    <input type="submit" class="btn btn-dark" value="Buscar" name="buscador">
                </div>
            </div> 
        </form>

        <form method="post" class="form-group col-md-6" acction="{$smarty.server.PHP_SELF}">
            <div class="input-group mb-3 form-row">
                <input type="text" class="form-control" placeholder="Introduce grupo (Ej. G1)" name="buscarG" aria-descr>
                <div class="input-group-append">
                    <input type="submit" class="btn btn-dark" value="Buscar" name="buscadorG">
                </div>
            </div> 
        </form>
    </div>
    {if isset($smarty.post.buscador) || isset($smarty.post.buscadorG)}
        {include 'admin/tablaClientes.tpl'}
    {/if}

</main>