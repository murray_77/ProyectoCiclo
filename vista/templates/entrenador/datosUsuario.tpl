<div class="capa2"></div>
<main class="contenedor datos">    
    <section class="encabezado">        
        <img class="userfoto" src="/proyecto/img/profiles/{$foto}" alt="Imagen Usuario"><!--Imagen actual del user o marco vacío-->
        <div class="titDetalles">
            <button type="button" id="botDatos" class="botonSimple active">Datos Personales</button>
            {if ($entrenador!=1)}
            <button type="button" id="botAnotaciones" class="botonSimple">Anotaciones</button>
            {/if}
        </div>
        <div class="linea2">&nbsp;</div>
    </section> 
        
            
    
    <section id="datos" class="columnaGrande">
        <div class="fila">
            <div class="columna">

                <div class="itemCaja">
                    <h4 class="etiqueta">Nombre</h4>
                    <span>{$nombre} {$apellido}</span>
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">DNI</h4>
                    <span>{$dni}</span>
                </div>

                {if ($entrenador!=1)}
                <div class="itemCaja">                
                    <h4 class="etiqueta">Fecha Nac.</h4>
                    <span>{$fechaN}</span>                
                </div>
                {/if}

            </div>


            <div class="columna">

                <div class="itemCaja">
                    <h4 class="etiqueta">Teléfono</h4>
                    <span>{$telefono}</span>
                </div>
                <div class="itemCaja">
                    <h4 class="etiqueta">Email</h4>
                    <span>{$email}</span>
                </div>
                {if ($entrenador!=1)}
                <div class="itemCaja">
                    <h4 class="etiqueta">Grupo</h4>
                    <span>{$grupo}</span>
                </div>
                {/if}
            </div>           

            {if ($entrenador!=1)}
             <div class="columna">   
                <div class="itemCaja">
                    <h4 class="etiqueta">Dirección</h4>
                    <span>{$direccion}</span>
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">Población</h4>
                    <span>{$poblacion}</span>                
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">CP</h4>
                    <span>{$cp}</span>
                </div>
             </div>

            {else}

            <div class="columna">    
                <div class="itemCaja">                    
                    <h4 class="etiqueta">Grupo</h4>
                    <span>{$grupo}</span>
                </div> 

                <div class="itemCaja">
                    <h4 class="etiqueta">Permisos de Administrador</h4>                
                    <span>{$permisos}</span>
                </div>  
            </div>
            {/if} 
        </div>    
    </section>
        
    <section id="anotaciones" class="columnaGrande">
        <div class="fila">
            <div class="columna">   
                <div class="itemCaja">
                    <h4 class="etiqueta">Nombre</h4>
                    <span>{$nombre} {$apellido}</span>
                </div>

                <div class="itemCaja">
                    <h4 class="etiqueta">DNI</h4>
                    <span>{$dni}</span>
                </div>

                {if ($entrenador!=1)}
                <div class="itemCaja">                
                    <h4 class="etiqueta">Fecha Nac.</h4>
                    <span>{$fechaN}</span>                
                </div>
                {/if}
            </div>    
        </div>
        <div class="columna">
            
        </div>
        
    </section>    

    {if isset($smarty.session.usuario)&&$smarty.session.usuario->administrador==1}            
    <section class="formulario">
        <form action="{$smarty.server.PHP_SELF}" method="post" class="botones">
            <input type="hidden" name="dni" value="{$dni}">
            <input type="submit" class="boton destacado" name="eliminarC" value="Eliminar">
            <input type="submit" class="boton destacado" value="Editar">
        </form>
    </section>
    {/if}
    
</main>