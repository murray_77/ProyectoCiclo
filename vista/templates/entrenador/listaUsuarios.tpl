<main class="contenedor">
    <div class="tituloBack">
        <h1>Lista de {$cabecera}</h1>

        <input type="text" class="busc" id="busc" onkeyup="buscar();" placeholder="Buscar Usuario" title="Buscar">
    </div>
        
    <div class="tablaWrap">    
        {include 'entrenador/tablaUsuarios.tpl'}
    </div>
    
    <div class="nUser">
        <a href={$direccion}><img src="/proyecto2.0/img/icons/nUser.png"></a>
    </div>

</main>
    