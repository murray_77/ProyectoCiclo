
<main class="contenedor">
    <h1 class="fila">Alta de Nuevo Cliente</h1>
    <form  class="formDatos" action="{$smarty.server.PHP_SELF}" method="post" onsubmit="return validaUsuario();">        
        <div id="datos" class="formulario">    
            <div class="columna">
                <div class="formGroup">          
                    <label class="etiqueta" for="nombre">Nombre<span class="obligatorio">*</span></label>
                    <input type="text" class="campoForm" id="nombre" name="nombre" autofocus> 
                    <div class="error" id="error1">&nbsp;</div>
                </div>            


                <div class="formGroup">
                    <label class="etiqueta" for="apellido">Apellidos<span class="obligatorio">*</span></label>
                    <input type="text" class="campoForm" id="apellido" name="apellido">
                    <div class="error" id="error2">&nbsp;</div>
                </div>

                <div class="formGroup">
                    <label class="etiqueta" for="dni">DNI<span class="obligatorio">*</span></label>
                    <input type="text" class="campoForm" id="dni" name="dni">
                    <div class="error" id="error3">&nbsp;</div>
                </div>            
            </div>

            <div class="columna">                

                <div class="formGroup">                 
                    <label class="etiqueta" for="em">Email<span class="obligatorio">*</span></label>
                    <input type="email" class="campoForm" id="email" name="email">
                    <div class="error" id="error6">&nbsp;</div>
                </div>

                <div class="formGroup">    
                    <label class="etiqueta" for="tele">Teléfono<span class="obligatorio">*</span></label>                
                    <input type="tel" class="campoForm" id="telefono" name="telefono">
                    <div class="error" id="error5">&nbsp;</div>
                </div> 

                <div class="formGroup">
                    <label class="etiqueta" for="fechan">Fecha Nacimiento</label>
                    <input type="date" class="campoForm" id="fechaN" name="fechaN" >
                    <div class="error" id="error7">&nbsp;</div>
                </div>
            </div>
            <div class="columna">
                <div class="formGroup">                 
                    <label class="etiqueta" for="dire">Direccion</label>
                    <input type="email" class="campoForm" id="dire" name="dire">
                    <div class="error" id="error8">&nbsp;</div>
                </div>

                <div class="formGroup">    
                    <label class="etiqueta" for="tele">Población</label>                
                    <input type="tel" class="campoForm" id="pob" name="pob">
                    <div class="error" id="error9">&nbsp;</div>
                </div> 

                <div class="formGroup">
                    <label class="etiqueta" for="fechan">CP</label>
                    <input type="text" class="campoForm" id="cp" name="cp" >
                    <div class="error" id="error10">&nbsp;</div>
                </div>            
            </div>
            
        </div>
             
        <div id="anotaciones" class="formulario">
            
            <div class="columna">
                <div class="formGroup">
                    <label class="etiqueta" for="grupo">Grupo<span class="obligatorio">*</span></label>
                    <select id="grupo" name="grupo" class="campoForm">
                        <option value=""></option>
                        <option value="G1">Grupo 1</option>
                        <option value="G2">Grupo 2</option>
                    </select>                
                    <div class="error" id="error4">&nbsp;</div>
                </div>
            

                <div class="formGroup">
                    <label class="etiqueta" for="condMed">Condiciones Médicas</label>
                    <textarea class="campoForm" rows="5" id="condMed" name="cMed"></textarea>
                </div>
            </div> 
            
            <div class="columna">
                <div class="formGroup">
                    <label class="etiqueta" for="objetivos">Objetivos</label>
                    <textarea class="campoForm" rows="10" id="objetivos" name="objetivos"></textarea>
                </div>
            </div>
             
            <div class="columna"> 
                <div class="formGroup">
                    <label class="etiqueta" for="anotaciones">Anotaciones</label>
                    <textarea class="campoForm" rows="10" id="anotaciones" name="anotaciones"></textarea>
                </div>
            </div>            
        </div>
        <div class="botones"">
            <button type="button" id="slide" class="destacado boton">Siguiente ></button>
            <input type="submit" id="aceptar" class="destacado boton oculto"  name="aceptar" value="Aceptar">                
        </div>
    </form>
    
</main>