<table id="tabus" class="tabus">        
    <thead>
        <tr>
            <th></th>
            <th>Nombre</th>
            <th>Apellidos</th>						
            <th>Grupo</th>
            <th>Teléfono</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>

{foreach $usuarios as $usuario}
    <tr class="usuTabla"> 
        <td><img class="flechaTabla" src="/proyecto2.0/img/icons/flecha.png"></td>
        <td>{$usuario->nombre}</td>
        <td>{$usuario->apellidos}</td>
        <td>{$usuario->grupo}</td>
        <td>{$usuario->telefono}</td>
        <td>{$usuario->email}</td>
        {if ($usuario->entrenador == 1)}
        <td><a href="/proyecto2.0/php/entrenador/detalles/datosUser.php?id={$usuario->id}&class=entrenador">Ver</a></td>
        {else}
        <td><a href="/proyecto2.0/php/entrenador/detalles/datosUser.php?id={$usuario->id}&class=cliente">Ver</a></td>
        {/if}
  </tr>
{/foreach}

    </tbody>
</table></br>