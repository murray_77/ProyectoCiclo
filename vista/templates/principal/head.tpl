<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>        
        <link rel="icon" href="/proyecto2.0/img/icons/logo_nav.png">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/fullcalendar/core/main.min.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/fullcalendar/timegrid/main.min.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/fullcalendar/daygrid/main.min.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/css/estilos.css">        
        <title id="titulo">{$titulo}</title>
    </head>
    <body>