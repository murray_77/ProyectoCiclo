<main class="container">
	<h1>Mapa de Navegación</h1>
	<div class="container">
            <div class="row">
                <div class="col-md-4">
                    <strong><a href="/proyecto/index.php">Inicio</a></strong>
                    <ul>
                        <li><a href="/proyecto/principal/horarios.php">Horarios</a></li>
                        <li><a href="/proyecto/principal/tarifas.php">Tarifas</a></li>
                        <li><a href="/proyecto/principal/contacto.php">Contacto</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <strong><a>Usuario</a></strong>
                    <ul>
                        <li><a href="/proyecto/cliente/portaluser.php">Perfil</a></li>
                        <li><a href="/proyecto/cliente/citasuser.php">Citas</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <strong><a>Administrador</a></strong>
                    <ul>
                        <li><a href="/proyecto/admin/portaladmin.php">Agenda</a></li>
                        <li>Clientes
                            <ul>
                                <li><a href="/proyecto/admin/subadmin/subaduser/registrocliente.php">Nuevo Cliente</a></li>
                                <li><a href="/proyecto/admin/subadmin/subaduser/listaClientes.php">Lista de Clientes</a></li>
                                <li><a href="/proyecto/admin/subadmin/subaduser/buscador.php">Buscador de clientes</a></li>
                            </ul>
                        </li>
                        <li>Entrenadores
                            <ul>
                                <li><a href="/proyecto/admin/subadmin/subadentrenador/registroentrenador.php">Nuevo Entrenador</a></li>
                                <li><a href="/proyecto/admin/subadmin/subadentrenador/listaentrenadores.php">Lista de Entrenadores</a></li>
                            </ul>
                        </li>
                        <li><a href="/proyecto/admin/subadmin/admingrupos.php">Grupos</a></li>
                        <li><a href="/proyecto/admin/subadmin/admincalendario.php">Calendario</a></li>
                    </ul>
                </div>
            </div>
	</div>
</main>