<?php
/* Smarty version 3.1.33, created on 2020-04-29 13:04:48
  from 'C:\xampp\htdocs\proyecto2.0\vista\templates\principal\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ea95f50a5bd20_53260340',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0a784a24960c406e6b4625e374f70feca11480e5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\proyecto2.0\\vista\\templates\\principal\\index.tpl',
      1 => 1588158248,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ea95f50a5bd20_53260340 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="">    
    <section id="inicio" class="inicio">
        <div class="capa"></div>
        <div class="logoWrap">
            <img class="logoPortada" src="/proyecto2.0/img/icons/logoInicio.png">
        </div> 

    </section>

    <section id="presentacion" class="presentacion">

        <div id="titulo" class="titulo">
            <h1>DESCUBRE EL MÉTODO <span class="negrita">CASAL</span></h1>
            <div class="linea">&nbsp;</div>
        </div>
        <div class="presColumnas">
            <div class="">
                <figure>
                    <img class="presIcon" src="/proyecto2.0/img/icons/pres1.png">
                </figure>
                <h3><span class="negrita">QUIEN SOY</span></h3>
                <p>Mi nombre es Jorge Casal. Soy entrenador personal y mi objetivo
                principal es evolucinar en mi sector</p>
            </div>
            <div class="">
                <figure>
                    <img class="presIcon" src="/proyecto2.0/img/icons/pres2.png">
                </figure>
                <h3><span class="negrita">ESTUDIOS</span></h3>
                <p>Mi nombre es Jorge Casal. Soy entrenador personal y mi objetivo
                principal es evolucinar en mi sector</p>
            </div>
            <div class="">
                <figure>
                    <img class="presIcon" src="/proyecto2.0/img/icons/pres3.png">
                </figure>
                <h3><span class="negrita">EXPERIENCIA</span></h3>
                <p>Mi nombre es Jorge Casal. Soy entrenador personal y mi objetivo
                principal es evolucinar en mi sector</p>
            </div>
        </div>

        <div class="slidePres">
            <img class="slideFotoPres" src="/proyecto2.0/img/in01.jpg">
            <img class="slideFotoPres" src="/proyecto2.0/img/in02.jpg">
            <img class="slideFotoPres" src="/proyecto2.0/img/in03.jpg">
            <img class="slideFotoPres" src="/proyecto2.0/img/in04.jpg">              
        </div>
        <div class="inclinado"></div>
    </section>

    <section id="tarifas" class="tarifas">
        <div class="tar1">
            
        </div>
        <div class="tar2">

        </div>
        <div class="tar3">

        </div>
    </section>

    <section id="blog" class="blog">
        <h1>BLOG</h1>
        <div class="slideBlog">
            <img class="slideFotoBlog" src="/proyecto2.0/img/blog01.jpg">
            <img class="slideFotoBlog" src="/proyecto2.0/img/blog02.jpg">
            <img class="slideFotoBlog"src="/proyecto2.0/img/blog03.jpg">         
        </div>
    </section>

    <section id="contacto" class="contacto">    

    </section>
</main><?php }
}
