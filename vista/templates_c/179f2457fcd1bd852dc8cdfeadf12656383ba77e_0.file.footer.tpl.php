<?php
/* Smarty version 3.1.33, created on 2020-03-11 17:20:01
  from 'C:\wamp64\www\proyecto2.0\vista\templates\principal\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e691dc1adaed4_94798406',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '179f2457fcd1bd852dc8cdfeadf12656383ba77e' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\principal\\footer.tpl',
      1 => 1583946906,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e691dc1adaed4_94798406 (Smarty_Internal_Template $_smarty_tpl) {
?>        <footer class="foot">
            <div class="foot01">
                <img class="logoFoot" src="/proyecto2.0/img/icons/logo_blanco.png">
            </div>
            <div class="foot02">
                <img class="redes" src="/proyecto2.0/img/icons/redes.png">
                <p>Copyright 2020, Todos los derechos reservados</p>
            </div>
            <div class="foot03">
                <ul>
                    <li><a href="#" class="enlaceFoot">PRIVACIDAD</a></li>
                    <li><a href="#" class="enlaceFoot">PREGUNTAS FRECUENTES</a></li>
                    <li><a href="#" class="enlaceFoot">COOKIES</a></li>
                    
                </ul>
            </div>
        </footer>

        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/jquery.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/core/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/daygrid/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/timegrid/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/interaction/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/proyecto2.0/slick/slick.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/script.js"><?php echo '</script'; ?>
>
    
    </body>
</html>
<?php }
}
