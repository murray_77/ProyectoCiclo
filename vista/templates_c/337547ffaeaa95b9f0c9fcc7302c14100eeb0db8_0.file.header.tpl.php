<?php
/* Smarty version 3.1.33, created on 2020-02-06 18:04:48
  from 'D:\wamp64\www\proyecto2.0\vista\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e3c55400c0386_69262490',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '337547ffaeaa95b9f0c9fcc7302c14100eeb0db8' => 
    array (
      0 => 'D:\\wamp64\\www\\proyecto2.0\\vista\\templates\\header.tpl',
      1 => 1581009703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:userFoto.tpl' => 1,
  ),
),false)) {
function content_5e3c55400c0386_69262490 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="es">
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <link rel="stylesheet" href="/proyecto2.0/css/estilos.css" media="screen">
            <link rel="stylesheet" href="/proyecto2.0/css/bootstrap.css" media="screen">
            <title id="titulo"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</title>
	</head>

	<body>
	
	<header>
            <div class="container-fluid">
                <?php if (isset($_SESSION['usuario'])) {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:userFoto.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                            
                <?php }?>
            </div>
            <img src="/proyecto2.0/img/head.jpg" alt="fotocabe" class="fotocabe"><!--imagen fondo cabecera-->         
        </header><?php }
}
