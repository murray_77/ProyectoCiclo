<?php
/* Smarty version 3.1.33, created on 2020-02-15 01:31:26
  from 'C:\wamp64\www\proyecto2.0\vista\templates\entrenador\tablaUsuarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4749ee4b93b5_71079476',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '396c6ad18b29d047228603fcba93a61287ab014d' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\entrenador\\tablaUsuarios.tpl',
      1 => 1581618102,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4749ee4b93b5_71079476 (Smarty_Internal_Template $_smarty_tpl) {
?><table id="tabus" class="table-responsive-sm table-bordered tabus">        
    <thead>
        <tr>
            <th>DNI</th>
            <th>Nombre</th>
            <th>Apellidos</th>						
            <th>Grupo</th>
            <th>Teléfono</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['usuarios']->value, 'usuario');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['usuario']->value) {
?>
    <tr> 
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->dni;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->nombre;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->apellidos;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->grupo;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->telefono;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->email;?>
</td>
        <?php if (($_smarty_tpl->tpl_vars['usuario']->value->entrenador == 1)) {?>
        <td><a href="/proyecto2.0/php/entrenador/detalles/datosUser.php?id=<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id;?>
&class=entrenador">Ver</a></td>
        <?php } else { ?>
        <td><a href="/proyecto2.0/php/entrenador/detalles/datosUser.php?id=<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id;?>
&class=cliente">Ver</a></td>
        <?php }?>
  </tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

    </tbody>
</table></br><?php }
}
