<?php
/* Smarty version 3.1.33, created on 2020-04-27 19:30:09
  from 'C:\xampp\htdocs\proyecto2.0\vista\templates\entrenador\tablaUsuarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ea716a11a3e02_08295303',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4be2987231b899da4dc3a6b629c1d0d14b5e25a1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\proyecto2.0\\vista\\templates\\entrenador\\tablaUsuarios.tpl',
      1 => 1588008555,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ea716a11a3e02_08295303 (Smarty_Internal_Template $_smarty_tpl) {
?><table id="tabus" class="tabus">        
    <thead>
        <tr>
            <th></th>
            <th>Nombre</th>
            <th>Apellidos</th>						
            <th>Grupo</th>
            <th>Teléfono</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['usuarios']->value, 'usuario');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['usuario']->value) {
?>
    <tr class="usuTabla"> 
        <td><img class="flechaTabla" src="/proyecto2.0/img/icons/flecha.png"></td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->nombre;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->apellidos;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->grupo;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->telefono;?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['usuario']->value->email;?>
</td>
        <?php if (($_smarty_tpl->tpl_vars['usuario']->value->entrenador == 1)) {?>
        <td><a href="/proyecto2.0/php/entrenador/detalles/datosUser.php?id=<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id;?>
&class=entrenador">Ver</a></td>
        <?php } else { ?>
        <td><a href="/proyecto2.0/php/entrenador/detalles/datosUser.php?id=<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id;?>
&class=cliente">Ver</a></td>
        <?php }?>
  </tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

    </tbody>
</table></br><?php }
}
