<?php
/* Smarty version 3.1.33, created on 2020-03-03 15:16:24
  from 'C:\wamp64\www\proyecto2.0\vista\templates\principal\nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5e74c8b95b81_70489423',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5ec18c8dd7321c46be73b5e3e7697487afebe521' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\principal\\nav.tpl',
      1 => 1583248582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e5e74c8b95b81_70489423 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar-expand-lg bg-transparent sticky">
    <div class="backNav"></div>
    <div class="navbar-header">
            <img class="logoNav navbar-brand" src="/proyecto2.0/icons/logo_nav.png" alt="logo">
        <button class="navbar-toggler collapsed custom-toggler" type="button" data-toggle="collapse" data-target="#desplegable" aria-controls="desplegable" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="desplegable">
        <ul class="nav navbar-nav">
            <li class="nav-item"><a id="inicio" class="nav-link" href="#">INICIO</a></li>
            <li class="nav-item"><a id="tarifas" class="nav-link" href="#">TARIFAS</a></li>
            <li class="nav-item"><a id="horarios" class="nav-link" href="#">BLOG</a></li>
            <li class="nav-item"><a id="contacto" class="nav-link" href="#">CONTACTO</a></li>

        <?php if (isset($_SESSION['usuario'])) {?>        
               
            <li class="nav-brand dropdown">
                <a class="nav-link dropdown-toggle p-0" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img class="rounded-circle fotohead" src="/proyecto2.0/img/profiles/<?php echo $_SESSION['usuario']->foto;?>
" />  
                </a>

                 <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary bg-dark" aria-labelledby="navbarDropdownMenuLink-55">
                    <?php if (isset($_SESSION['usuario']->entrenador) && $_SESSION['usuario']->entrenador == 1) {?>
                        <a class="nav-link" href="/proyecto2.0/php/entrenador/agenda.php">ADMINISTRAR</a>
                    <?php } else { ?>
                        <a class="nav-link" href="/proyecto2.0/php/cliente/agendaCliente.php">USUARIO</a>
                    <?php }?>
                    <a class="nav-link destacado" href="/proyecto2.0/clases/logoff.php">CERRAR SESION</a>
                </div>
            </li>

        <?php } else { ?>

            <li class="nav-item"><a class="nav-link destacado" name="ventana" data-toggle="modal" data-target="#modal" href="#">LOGIN</a></li>                
        </ul>
        <?php }?>           
    </div>
</nav><?php }
}
