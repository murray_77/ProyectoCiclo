<?php
/* Smarty version 3.1.33, created on 2020-02-07 19:25:11
  from 'D:\wamp64\www\proyecto2.0\vista\templates\entrenador\datosUsuario.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e3db997b2add2_32620952',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '81514cefa30220d06efaa5d9e5ff386f62cd2165' => 
    array (
      0 => 'D:\\wamp64\\www\\proyecto2.0\\vista\\templates\\entrenador\\datosUsuario.tpl',
      1 => 1581103283,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3db997b2add2_32620952 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="container">
    <section>
        <article class="per">
            <h2>Datos Personales</h2>
            <div class="foto"><img class="userfoto rounded-circle" src="/proyecto/img/profiles/<?php echo $_smarty_tpl->tpl_vars['foto']->value;?>
" alt="Imagen Usuario"></div><!--Imagen actual del user o marco vacío-->
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Nombre: </strong><?php echo $_smarty_tpl->tpl_vars['nombre']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Apellidos:</strong> <?php echo $_smarty_tpl->tpl_vars['apellido']->value;?>
</div></br>
                    <div class="col-md-12"><strong>DNI:</strong> <?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
</div>
                </div>
                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Fecha de Nacimiento:</strong> <?php echo $_smarty_tpl->tpl_vars['fechaN']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Grupo:</strong> <?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
</div></span></br>
                </div>
                <?php } else { ?>
                <div class="col-md-6">                    
                    <div class="col-md-12"><strong>Grupo:</strong> <?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Permisos de Administrador:</strong> <?php echo $_smarty_tpl->tpl_vars['permisos']->value;?>
</div>
                </div>    
                <?php }?>    
            </div>
        </article></br>

        <article class="cont">
            <h2>Datos de Contacto</h2>
            <div class="row">
                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Dirección:</strong> <?php echo $_smarty_tpl->tpl_vars['direccion']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Población:</strong> <?php echo $_smarty_tpl->tpl_vars['poblacion']->value;?>
</div></br>
                    <div class="col-md-12"><strong>CP:</strong> <?php echo $_smarty_tpl->tpl_vars['cp']->value;?>
</div></br>
                </div>
                <?php }?>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Teléfono:</strong> <?php echo $_smarty_tpl->tpl_vars['telefono']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Email:</strong> <?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</div></br>
                </div>
            </div>
        </article>
    </section>
    <?php if (isset($_SESSION['usuario']) && $_SESSION['usuario']->administrador == 1) {?>            
    <section class="botones">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="post">
            <input type="hidden" name="dni" value="<?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
">
            <input type="submit" class="btn btn-dark" name="eliminarC" value="Eliminar">
            <input type="submit" class="btn btn-dark" value="Editar">
        </form>
    </section>
    <?php }?>
</main><?php }
}
