<?php
/* Smarty version 3.1.33, created on 2020-02-11 00:07:31
  from 'C:\wamp64\www\proyecto2.0\vista\templates\cliente\datosCliente.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e41f04384b7c5_56361415',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '862adbb1bd9d097b7e291011ddd00f0dc19954b8' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\cliente\\datosCliente.tpl',
      1 => 1581104473,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e41f04384b7c5_56361415 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="container">
    <section>
        <article class="per">
            <h2>Datos Personales</h2>
            <div class="foto"><img class="userfoto rounded-circle" id="userfoto" src="/proyecto/img/profiles/<?php echo $_SESSION['usuario']['foto'];?>
" alt="Imagen Usuario"></div><!--Imagen actual del user o marco vacío-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-row"><strong>Nombre: </strong><?php echo $_SESSION['usuario']['nombre'];?>
</div></br><!--En los span vacíos se importan los datos de usuario desde la base de datos--> 
                    <div class="form-row"><strong>Apellidos:</strong> <?php echo $_SESSION['usuario']['apellido'];?>
</div></br>
                    <div class="form-row"><strong>DNI:</strong> <?php echo $_SESSION['usuario']['dni'];?>
</div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Fecha de Nacimiento:</strong> <?php echo $_SESSION['usuario']['fechaN'];?>
</div></br>
                    <div class="col-md-12"><strong>Grupo:</strong> <?php echo $_SESSION['usuario']['grupo'];?>
</div></span></br>
                    <form method="post" enctype="multipart/form-data" id="subirfoto" action="" class="col-md-6">
                        <div class="col-form-label"><label for="foto"><strong>Cambiar Foto de Perfil:</strong></label></div>
                        <div class="form-row"><input type="file" name="foto" id="foto"><div class="invalid-feedback"></div></div></br>
                        <div class="from-row"><input type="submit" value="Enviar" name="enviar" class="btn btn-dark"></div>
                    </form>
                </div>
            </div>
        </article></br>

        <article class="cont">
            <h2>Datos de Contacto</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Dirección:</strong> <?php echo $_SESSION['usuario']['direccion'];?>
</div></br><!--En los span vacíos se importan los datos de usuario desde la base de datos--> 
                    <div class="col-md-12"><strong>Población:</strong> <?php echo $_SESSION['usuario']['poblacion'];?>
</div></br>
                    <div class="col-md-12"><strong>CP:</strong> <?php echo $_SESSION['usuario']['cp'];?>
</div></br>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Teléfono:</strong> <?php echo $_SESSION['usuario']['telefono'];?>
</div></br>
                    <div class="col-md-12"><strong>Email:</strong> <?php echo $_SESSION['usuario']['email'];?>
</div></br>
                </div>
            </div>
        </article>
    </section>
    <section class="botones">
            <form method="post" action="php" action="">
                    <input type="submit" class="btn btn-dark" value="Cambiar Contraseña">
                    <input type="submit" class="btn btn-dark" value="Solicitar Baja">
            </form>
    </section>
</main><?php }
}
