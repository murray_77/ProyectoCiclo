<?php
/* Smarty version 3.1.33, created on 2020-03-06 23:02:47
  from 'C:\wamp64\www\proyecto2.0\vista\templates\principal\navBar.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e62d6977e5805_80077179',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '87584534b6bbf77150a5c8d426cf4b9b8463146a' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\principal\\navBar.tpl',
      1 => 1583518604,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e62d6977e5805_80077179 (Smarty_Internal_Template $_smarty_tpl) {
?><header>
    <nav class="nav">
        <div class="backNav"></div>
        <div class="contenedorNav">
            <img class="logoNav" src="/proyecto2.0/img/icons/logo_nav.png" alt="logo">

            <div class="navBoton" onclick="cambiar(this);">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>    
        <ul class="main-nav" id="menu">
            <li><a href="#presentacion" class="enlaceNav en1">INICIO</a></li>
            <li><a href="#tarifas" class="enlaceNav en1">TARIFAS</a></li>
            <li><a href="#blog" class="enlaceNav en1">BLOG</a></li>
            <li><a href="#contacto" class="enlaceNav en1">CONTACTO</a></li> 
            
        <?php if (isset($_SESSION['usuario'])) {?>                        
            <?php if (isset($_SESSION['usuario']->entrenador) && $_SESSION['usuario']->entrenador == 1) {?>
                <li><a class="enlaceNav en1" href="/proyecto2.0/php/entrenador/agenda.php">ADMINISTRAR</a></li>
            <?php } else { ?>
                <li><a class="enlaceNav en1" href="/proyecto2.0/php/cliente/agendaCliente.php">USUARIO</a></li>
            <?php }?>
            <li><a class="destacado" href="/proyecto2.0/clases/logoff.php">CERRAR SESION</a></li>
            <li><img class="rounded-circle fotoNav" src="/proyecto2.0/img/profiles/<?php echo $_SESSION['usuario']->foto;?>
"/></li>            
        <?php } else { ?>
            <li><a class="destacado" id="login" href="#">LOGIN</a></li>
        <?php }?>           
                    
        </ul>        
    </nav>
</header><?php }
}
