<?php
/* Smarty version 3.1.33, created on 2020-02-10 18:26:20
  from 'D:\wamp64\www\proyecto2.0\vista\templates\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e41a04ccaca71_26533065',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e6fd386dd3e4332e4e476afdc67108abffba8fa' => 
    array (
      0 => 'D:\\wamp64\\www\\proyecto2.0\\vista\\templates\\footer.tpl',
      1 => 1581359162,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e41a04ccaca71_26533065 (Smarty_Internal_Template $_smarty_tpl) {
?>    <footer class="section footer-classic context-dark bg-image pie"></br>
            <div class="container">
                <div class="row row-30">
                    <div class="col-md-4 col-xl-5">
                        <div class="pr-xl-4">
                            <!--<a class="brand" href="index.html"><img class="brand-logo-light" src="" alt="" width="140" height="37"></a>-->
                            <p>Última actualización: 28/05/2019</p>
	
                            <p class="rights"><span>©  </span><span class="copyright-year">2019</span><span> </span><span>SantiagoGC</span><span>. </span><span>Todos los Derechos Reservados.</span></p>
				<p>
                                    <a href="http://jigsaw.w3.org/css-validator/check/referer">
                                        <img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss-blue" alt="Valid CSS!" />
                                    </a>
				</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5>Contacto</h5>
                        <dl class="contact-list">
                            <dt>Dirección:</dt>
                            <dd>Marqués de Figueroa, Fene, A Coruña</dd>
                        </dl>
                        <dl class="contact-list">
                            <dt>email:</dt>
                            <dd><a href="mailto:#">unadireccion@gmail.com</a></dd>
                        </dl>
                        <dl class="contact-list">
                            <dt>Teléfono:</dt>
                            <dd><a href="tel:#">+34 7568543012</a> <span>or</span> <a href="tel:#">+34 9571195353</a></dd>
                        </dl>
                    </div>
                    <div class="col-md-4 col-xl-3">
                        <h5>Enlaces</h5>
                            <ul class="nav-list">
                                <li><a href="/proyecto/index.php">Inicio</a></li>
                                <li><a href="#">Politica de Privacidad</a></li>
                                <li><a href="/proyecto/principal/mapanavegacion.php">Mapa de Navegación</a></li>
                            </ul>
                    </div>
                </div>
            </div>    
        </footer>
		<?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/jquery.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/popper.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/bootstrap.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/script.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
