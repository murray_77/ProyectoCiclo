<?php
/* Smarty version 3.1.33, created on 2020-02-04 18:14:23
  from 'C:\wamp\www\proyecto2.0\vista\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e39b47f75fc32_30363585',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '96433ca40f93399fe96806dfb4929114a6555593' => 
    array (
      0 => 'C:\\wamp\\www\\proyecto2.0\\vista\\templates\\header.tpl',
      1 => 1580840061,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:userFoto.tpl' => 1,
  ),
),false)) {
function content_5e39b47f75fc32_30363585 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="es">
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <link rel="stylesheet" href="/proyecto2.0/css/estilos.css" media="screen">
            <link rel="stylesheet" href="/proyecto2.0/css/bootstrap.css" media="screen">
            <title id="titulo"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</title>
	</head>

	<body>
	
	<header>
            <div class="container-fluid">
                <?php if (isset($_SESSION['usuario'])) {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:userFoto.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                            
                <?php }?>
            </div>
            <img src="/proyecto2.0/img/head.jpg" alt="fotocabe" class="fotocabe"><!--imagen fondo cabecera-->         
        </header><?php }
}
