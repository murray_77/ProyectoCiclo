<?php
/* Smarty version 3.1.33, created on 2020-02-06 18:22:38
  from 'D:\wamp64\www\proyecto2.0\vista\templates\entrenador\nuevoCliente.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e3c596eadef95_07645567',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a4337fcd707f2739f6824d2ab94c1a3ff77d877f' => 
    array (
      0 => 'D:\\wamp64\\www\\proyecto2.0\\vista\\templates\\entrenador\\nuevoCliente.tpl',
      1 => 1581009703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3c596eadef95_07645567 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="container">
    <h1 class="">Alta de Nuevo Cliente</h1>
    <form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="post" onsubmit="return validaUsuario();">
        <h3>Datos Personales</h3>
        <div class="form-row">
            <div class="form-group col-md-6">	
                <label for="nombre">Nombre<span class="obligatorio">*</span></label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="nombre" name="nombre" autofocus>                
                <div class="invalid-feedback" id="error1"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="apellido">Apellidos<span class="obligatorio">*</span></label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="apellido" name="apellido">
                <div class="invalid-feedback" id="error2"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="dni">DNI<span class="obligatorio">*</span></label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Ocho caracteres numéricos y una letra">
                <input type="text" class="form-control" id="dni" name="dni">
                <div class="invalid-feedback" id="error3"></div>
            </div>            
            <div class="form-group col-md-6">
                <label for="fechan">Fecha Nacimiento</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Elije la fecha de nacimiento">
                <input type="date" class="form-control" id="fechaN" name="fechaN" >
                <div class="invalid-feedback" id="error7"></div>
            </div>
            <div class="form-group col-md-6">    
                <label for="em">Email<span class="obligatorio">*</span></label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="ej: usuario@miemail.com, Se utilizará como nombre de usuario">
                <input type="email" class="form-control" id="email" name="email">
                <div class="invalid-feedback" id="error6"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="grupo">Grupo<span class="obligatorio">*</span></label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Grupo asignado al cliente">
                <select id="grupo" class="form-control">
                    <option value=""></option>
                    <option value="G1">Grupo 1</option>
                    <option value="G2">Grupo 2</option>
                </select>                
                <div class="invalid-feedback" id="error4"></div>
            </div>
        </div>
        <h3>Contacto</h3>
        <div class="form-row">            
            <div class="form-group col-md-6">
                <label for="dire">Dirección</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Calle y número de vivienda">
                <input type="text" class="form-control" id="dire" name="dire">
                <div class="invalid-feedback" id="error8"></div>
            </div>    
            <div class="form-group col-md-6">
                <label for="pob">Población</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Solo letras mayúsculas o minúsculas">
                <input type="text" class="form-control" id="pob" name="pob">
                <div class="invalid-feedback" id="error9"></div>
            </div>    
            <div class="form-group col-md-6">    
                <label for="cp">CP</label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Cinco caracteres numéricos">
                <input type="text" class="form-control" id="cp" name="cp">
                <div class="invalid-feedback" id="error10"></div>
            </div>
            <div class="form-group col-md-6">    
                <label for="tele">Teléfono<span class="obligatorio">*</span></label>
                <img class="info" src="/proyecto/img/question.png" data-toggle="tooltip" data-placement="right" title="Nueve caracteres numéricos">
                <input type="tel" class="form-control" id="telefono" name="telefono">
                <div class="invalid-feedback" id="error5"></div>
            </div>    
        </div>
        <h3>Anotaciones</h3>
        <div class="form-group row">
            <div class="col-sm-6">
                <label for="condMed">Condiciones Médicas</label>
                <textarea class="form-control" rows="5" id="condMed"></textarea>
            </div>
            <div class="col-sm-6">
                <label for="objetivos">Objetivos</label>
                <textarea class="form-control" rows="5" id="objetivos"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <label for="anotaciones">Anotaciones</label>
                <textarea class="form-control" rows="5" id="anotaciones"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <input type="submit" class="btn btn-dark" id="aceptar" name="aceptar" value="Aceptar">
            </div>
        </div>
    </form>
    
</main><?php }
}
