<?php
/* Smarty version 3.1.33, created on 2020-03-11 17:20:01
  from 'C:\wamp64\www\proyecto2.0\vista\templates\principal\head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e691dc1a36695_86387869',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b17e229e6e26bf049c20b802ebac2e4217ec28e1' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\principal\\head.tpl',
      1 => 1583946770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e691dc1a36695_86387869 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>        
        <link rel="icon" href="/proyecto2.0/img/icons/logo_nav.png">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/fullcalendar/core/main.min.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/fullcalendar/timegrid/main.min.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/fullcalendar/daygrid/main.min.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="/proyecto2.0/css/estilos.css">        
        <title id="titulo"><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</title>
    </head>
    <body><?php }
}
