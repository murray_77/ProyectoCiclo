<?php
/* Smarty version 3.1.33, created on 2020-04-30 17:38:47
  from 'C:\xampp\htdocs\proyecto2.0\vista\templates\principal\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5eaaf107abfbe7_20045377',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd1fd8efaadc0c1d49d905ccfa81afbf12b647d4d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\proyecto2.0\\vista\\templates\\principal\\footer.tpl',
      1 => 1588261122,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5eaaf107abfbe7_20045377 (Smarty_Internal_Template $_smarty_tpl) {
?>        <footer class="foot">
            <div class="foot01">
                <img class="logoFoot" src="/proyecto2.0/img/icons/logo_solo_blanco.png">
            </div>
            <div class="foot02">
                <img class="redes" src="/proyecto2.0/img/icons/redes.png">
                <p>Copyright 2020, Todos los derechos reservados</p>
            </div>
            <div class="foot03">
                <a href="#" class="enlaceFoot">PRIVACIDAD</a></li>
                <a href="#" class="enlaceFoot">PREGUNTAS FRECUENTES</a>
                <a href="#" class="enlaceFoot">COOKIES</a>
            </div>
        </footer>

        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/jquery.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/jquery-ui-1.12.1/jquery-ui.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/core/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/core/locales/es.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/daygrid/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/timegrid/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/fullcalendar/interaction/main.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="/proyecto2.0/slick/slick.min.js"><?php echo '</script'; ?>
><!--slider-->
        <?php echo '<script'; ?>
 type="application/javascript" src="/proyecto2.0/js/script.js"><?php echo '</script'; ?>
>
    
    </body>
</html>
<?php }
}
