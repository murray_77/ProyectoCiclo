<?php
/* Smarty version 3.1.33, created on 2020-02-06 18:04:48
  from 'D:\wamp64\www\proyecto2.0\vista\templates\nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e3c5540135cc9_40016754',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e063f3ee6c4a3a5c0a084b30926a389cc79fd8c4' => 
    array (
      0 => 'D:\\wamp64\\www\\proyecto2.0\\vista\\templates\\nav.tpl',
      1 => 1581009703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3c5540135cc9_40016754 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img class="minilogo navbar-brand" src="/proyecto2.0/img/logo.png" alt="logo">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a id="inicio" class="nav-link" href="<?php echo $_SERVER['DOCUMENT_ROOT'];?>
/proyecto2.0/index.php">Inicio</a></li>
            <li class="nav-item"><a id="tarifas" class="nav-link" href="/proyecto2.0/principal/tarifas.php">Tarifas</a></li>
            <li class="nav-item"><a id="horarios" class="nav-link" href="/proyecto2.0/principal/horarios.php">Horarios</a></li>
            <li class="nav-item"><a id="contacto" class="nav-link" href="/proyecto2.0/principal/contacto.php">Contacto</a></li>
        </ul>
            <?php if (isset($_SESSION['usuario'])) {?>
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="/proyecto2.0/clases/logoff.php">Cerrar Sesión</a></li>
                
                <?php if (isset($_SESSION['usuario']->entrenador) && $_SESSION['usuario']->entrenador == 1) {?>
                    <li class="nav-item"><a class="nav-link" href="/proyecto2.0/php/entrenador/agenda.php">Administrar</a></li>
                </ul>
                <?php } else { ?>
                    <li class="nav-item"><a class="nav-link" href="/proyecto2.0/php/cliente/portaluser.php">Usuario</a></li>
                </ul>
            <?php }?>
            <?php } else { ?>
                <form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="post" class="form-inline">
                    <input type="checkbox" name="recordar" value="1" class="form-check" checked>
                    <label class="form-check-label label-login" for="recordar"> Recordar usuario?</label>
                    <input type="text" maxlength="20" size="20" name="user" class="form-control mr-sm-2" placeholder="Usuario" value="">
                    <input type="password" maxlength="20" size="20" name="pass" class="form-control mr-sm-2" placeholder="Contraseña" value="">                    
                    <input type="submit" value="Acceder" name="acceder" id="acceder" class="btn btn-secondary">
                </form>
            <?php }?>
            
            </ul>
	</div>
</nav><?php }
}
