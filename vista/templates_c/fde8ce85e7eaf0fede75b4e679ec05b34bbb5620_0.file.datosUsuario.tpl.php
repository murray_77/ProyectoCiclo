<?php
/* Smarty version 3.1.33, created on 2020-02-11 00:54:27
  from 'C:\wamp64\www\proyecto2.0\vista\templates\principal\datosUsuario.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e41fb4369b7a2_47423222',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fde8ce85e7eaf0fede75b4e679ec05b34bbb5620' => 
    array (
      0 => 'C:\\wamp64\\www\\proyecto2.0\\vista\\templates\\principal\\datosUsuario.tpl',
      1 => 1581382465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e41fb4369b7a2_47423222 (Smarty_Internal_Template $_smarty_tpl) {
?><main class="container">
    <section>
        <article class="per">
            <h2>Datos Personales</h2>
            <div class="foto"><img class="userfoto rounded-circle" src="/proyecto/img/profiles/<?php echo $_smarty_tpl->tpl_vars['foto']->value;?>
" alt="Imagen Usuario"></div><!--Imagen actual del user o marco vacío-->
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Nombre: </strong><?php echo $_smarty_tpl->tpl_vars['nombre']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Apellidos:</strong> <?php echo $_smarty_tpl->tpl_vars['apellido']->value;?>
</div></br>
                    <div class="col-md-12"><strong>DNI:</strong> <?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
</div>
                </div>
                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Fecha de Nacimiento:</strong> <?php echo $_smarty_tpl->tpl_vars['fechaN']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Grupo:</strong> <?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
</div></span></br>
                </div>
                <?php } else { ?>
                <div class="col-md-6">                    
                    <div class="col-md-12"><strong>Grupo:</strong> <?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Permisos de Administrador:</strong> <?php echo $_smarty_tpl->tpl_vars['permisos']->value;?>
</div>
                </div>    
                <?php }?>    
            </div>
        </article></br>

        <article class="cont">
            <h2>Datos de Contacto</h2>
            <div class="row">
                <?php if (($_smarty_tpl->tpl_vars['entrenador']->value != 1)) {?>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Dirección:</strong> <?php echo $_smarty_tpl->tpl_vars['direccion']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Población:</strong> <?php echo $_smarty_tpl->tpl_vars['poblacion']->value;?>
</div></br>
                    <div class="col-md-12"><strong>CP:</strong> <?php echo $_smarty_tpl->tpl_vars['cp']->value;?>
</div></br>
                </div>
                <?php }?>
                <div class="col-md-6">
                    <div class="col-md-12"><strong>Teléfono:</strong> <?php echo $_smarty_tpl->tpl_vars['telefono']->value;?>
</div></br>
                    <div class="col-md-12"><strong>Email:</strong> <?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</div></br>
                </div>
            </div>
        </article>
    </section>
    <?php if (isset($_SESSION['usuario']->administrador) && $_SESSION['usuario']->administrador == 1) {?>            
    <section class="botones">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="post">
            <input type="hidden" name="dni" value="<?php echo $_smarty_tpl->tpl_vars['dni']->value;?>
">
            <input type="submit" class="btn btn-dark" name="eliminarC" value="Eliminar">
            <input type="submit" class="btn btn-dark" value="Editar">
        </form>
    </section>
    <?php }?>
</main><?php }
}
